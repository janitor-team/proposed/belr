Source: belr
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Bernhard Schmidt <berni@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libbctoolbox-dev (>= 5.0.37~)
Standards-Version: 4.6.0
Rules-Requires-Root: no
Section: libs
Homepage: https://www.linphone.org
Vcs-Git: https://salsa.debian.org/pkg-voip-team/linphone-stack/belr.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/linphone-stack/belr

Package: libbelr-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libbelr1 (= ${binary:Version}), ${misc:Depends}
Description: language recognition library by Belledonne Communications (development headers)
 Belr is Belledonne Communications' language recognition library. It aims
 at parsing any input formatted according to a language defined by an
 ABNF grammar, such as the protocols standardized at IETF.
 .
 It is based on finite state machine theory and heavily relies on
 recursivity from an implementation standpoint.
 .
 The package is probably not useful outside the Belledonne Communications suite
 of libraries and programs.
 .
 This package contains the development headers.

Package: libbelr1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: language recognition library by Belledonne Communications
 Belr is Belledonne Communications' language recognition library. It aims
 at parsing any input formatted according to a language defined by an
 ABNF grammar, such as the protocols standardized at IETF.
 .
 It is based on finite state machine theory and heavily relies on
 recursivity from an implementation standpoint.
 .
 The package is probably not useful outside the Belledonne Communications suite
 of libraries and programs.
 .
 This package contains the shared library.
